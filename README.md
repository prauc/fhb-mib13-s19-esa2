# Objektorientierte Skriptsprachen (FHBrbg MIB 13 S19)

## ESA2: Hit the Mole (PyGame)

_Hit the Mole_ is a small PyGame application, based on the well-known funfair game.
Tested on Ubuntu 18.10, Python 3.6.8

### Installing

You have to install several dependencies, before you can run this game.
All dependencies are mentioned within `requirements.txt`.
To install, please run `pip3 install -r requirements.txt`

### Usage

The game can be started by running `main.py` via your CLI.

### Known Issues

* [Pygame not compatible with MacOS Mojave](https://github.com/pygame/pygame/issues/555)

## Author

Created by **Patrick Rauchfuss**, SS19, Beuth Hochschule fuer Technik
26.05.2019