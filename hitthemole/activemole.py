

class ActiveMole:
    """ActiveMole Class
        needed to determine, which Mole is currently active

        Args:
            holes (Holes): DependencyInjection of Holes Class
    """

    def __init__(self, holes):
        self.holes = holes
        # get first active hole
        self.active_hole = self.holes.get_random()
        # no active mole so far
        self.active = None
        # determine mole state
        self.state = 0
        self.vanish = False
        self.hit = False

    def check_status(self):
        if self.hit:
            # vanish active mole
            self.vanish = True

            # show bump
            if self.state < 3:
                self.state = 3

            if self.state == 6:
                self.active = None
                self.vanish = False
                self.hit = False
                self.state = 0

            else:
                self.state += 1

        else:
            # start to appear
            if self.state == 0 and self.vanish:
                self.active = None
                self.vanish = False
            # mole to appear
            elif self.state < 2 and not self.vanish:
                self.state += 1
            # mole to disappear
            elif self.state < 2 and self.vanish:
                self.state -= 1
            # start to disappear
            elif self.state == 2:
                self.vanish = True
                self.state -= 1

        # determine new (random) active mole
        if self.active is None:
            self.active_hole = self.holes.get_random()
