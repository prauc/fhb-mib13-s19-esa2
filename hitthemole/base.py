

class Base:
    """Base Class
        all Actions rely on this Base-Class. Every Action needs at least a update- and click-method
    """

    MOUSE_BUTTON_DOWN = 5
    LEFT_MOUSE_BUTTON = 1

    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    BLACK = (0, 0, 0)
    BORDEAUX = (128, 0, 50)

    # every game sub-action needs at least an update-method
    def update(self):
        pass

    # every game sub-action needs at least an click-method
    def click(self):
        pass
