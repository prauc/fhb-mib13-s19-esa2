

class Cycle:
    """Cycle Class
        this class determine a pygame clock cycle
        different cycles may need different reaction times, so this abstract wrapper has been created

        Args:
            counter (Int): ms-counter
            interval (Int): determine, whenever a new interval should start
    """

    def __init__(self, counter, interval):
        self.counter = counter
        self.interval = interval

    # add new tick to counter
    def tick(self, sec):
        self.counter += sec

    # check if counter has reached new interval
    def reached(self):
        return self.counter > self.interval

    # reset counter
    def reset(self):
        self.counter = 0
