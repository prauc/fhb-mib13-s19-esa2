from pygame import *

import pygame


class Fonts:
    """Fonts Class
        available game fonts are stored here.
        They can be used for different purpose in the game

        Args:
            game (Game): DI-Container for Game-Class
    """
    def __init__(self, game):
        self.fonts = {
            'AR26': pygame.font.SysFont('Arial', 26),
            'AR48': pygame.font.SysFont('Arial', 48)
        }

    # possibility to add a new font
    def add(self, name, path):
        self.fonts.update({
            name: path
        })

    # get certain font
    def get(self, name):
        return self.fonts.get(name)
