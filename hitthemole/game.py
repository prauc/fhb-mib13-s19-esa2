from pygame import *

from .startscreen import Startscreen
from .match import Match
from .cycle import Cycle
from .fonts import Fonts

import pygame
import sys


class Game:
    """Game Class
        basic game class. All actions will be initiate by this class.

        Args:
            config (Dict): Configs provided by config.ini
    """
    def __init__(self, config):
        # store config obj
        self.config = config

        # int width and height
        width = self.config['game'].getint('width', 800)
        height = self.config['game'].getint('height', 600)

        pygame.init()

        self.fps = self.config['game'].getint('fps', 60)
        self.name = self.config['game'].get('name', "Hit the mole")
        self.fonts = Fonts(self)
        self.window = (width, height)
        self.screen = pygame.display.set_mode(self.window)
        self.highscore = 0

        pygame.display.set_caption(self.name)

        self.started = False
        self.action = self.get_startscreen()

    # get startscreen action
    def get_startscreen(self):
        self.started = False
        return Startscreen(self)

    # get match action
    def get_match(self):
        self.started = True
        return Match(self)

    # run a new game, entrypoint
    def run(self):
        # we need seperate cycles for roundtime and moles due different update reaction times
        cycles = {
            'roundtime': Cycle(0, 1),
            'mole': Cycle(0, .1)
        }

        clock = pygame.time.Clock()

        # while game is running
        while True:
            for event in pygame.event.get():
                if event.type == self.action.MOUSE_BUTTON_DOWN and event.button == self.action.LEFT_MOUSE_BUTTON:
                    self.action.click(pygame.mouse)

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

            # new match has started
            if self.started:
                # update mole time tick
                cycles['mole'].tick(clock.tick(self.fps) / 1000.0)
                # update roundtime time tick
                cycles['roundtime'].tick(clock.tick(self.fps) / 1000.0)

                # check if mole can be updated
                if cycles['mole'].reached():
                    self.action.update_mole()

                    cycles['mole'].reset()

                # check if roundtime can be updated
                if cycles['roundtime'].reached():
                    self.action.update_roundtime()
                    if self.action.roundtime <= 0:
                        if self.action.score > self.highscore:
                            self.highscore = self.action.score
                        self.action = self.get_startscreen()

                    cycles['roundtime'].reset()

            pygame.display.flip()
