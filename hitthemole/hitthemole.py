from configparser import *
from .game import Game


class HitTheMole:
    """HitTheMole Class
        Main Class for starting the Game
    """

    def __init__(self):
        config = ConfigParser()
        config.read('config.ini')

        Game(config).run()


def main():
    HitTheMole()


if __name__ == "__main__":
    main()
