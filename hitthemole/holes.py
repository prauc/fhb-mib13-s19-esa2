import random


class Holes:
    """Holes Class
        determine the coordinates of all holes on the match
    """
    def __init__(self):
        self.list = [
            (85, 15), (105, 412), (140, 179), (350, 310), (605, 438), (410, 119), (597, -3), (625, 238)
        ]

    # get random hole
    def get_random(self):
        return self.list[random.randint(0, self.length())]

    # return num of all holes
    def length(self):
        return self.list.__len__() - 1
