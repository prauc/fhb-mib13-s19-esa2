from pygame import *

from .moles import Moles
from .holes import Holes
from .activemole import ActiveMole
from .base import Base

import pygame


class Match(Base):
    """Match Class
        Start a new match

        Args:
            game (Game): DI-Container of Game-Class
    """
    def __init__(self, game):
        self.game = game
        self.font = game.fonts.get('AR26')
        self.background = pygame.image.load(game.config['match'].get('background'))

        # every new match starts with score 0
        self.score = 0
        # get roundtime from config
        self.roundtime = game.config['match'].getint('roundtime')
        # back button, if a player wants to cancel
        self.backbutton = None

        # get all mole states
        self.moles = Moles(game)
        # generate first active mole
        self.active_mole = ActiveMole(Holes())

        # update the pitch
        self.update()

    def update(self):
        # create new background graphic
        self.game.screen.blit(self.background, (0, 0))
        # draw all necessary objects on background
        self.draw_backbutton()
        self.draw_score()
        self.draw_roundtime()
        self.draw_mole()

    def update_mole(self):
        # update mole status (appear, disappear, bumped, ...)
        self.active_mole.check_status()
        self.update()

    def draw_mole(self):
        # draw new mole state on pitch
        pic = self.moles.get_mole_by_state(self.active_mole.state)
        # save active mole rect
        self.active_mole.active = self.game.screen.blit(pic, self.active_mole.active_hole)

    def update_roundtime(self):
        # update time left on current round
        self.roundtime -= 1
        self.update()

    def draw_backbutton(self):
        # create arrow back button
        text = self.game.fonts.get('AR26').render("\u2190", True, (255, 255, 255))
        rect = text.get_rect()
        rect.centerx = 25
        rect.centery = 14

        self.backbutton = self.game.screen.blit(text, rect)

    def draw_roundtime(self):
        # draw time left on current round on pitch
        text = self.font.render("Time left: {}s".format(self.roundtime), True, (255, 255, 255))

        rect = text.get_rect()
        rect.centerx = 150
        rect.centery = 18

        self.game.screen.blit(text, rect)

    def draw_score(self):
        # draw current score on pitch
        text = self.font.render("Score: {}".format(self.score), True, (255, 255, 255))

        rect = text.get_rect()
        rect.centerx = self.background.get_rect().centerx
        rect.centery = 18

        self.game.screen.blit(text, rect)

    def click(self, mouse):
        # if a player hits cancel, then end current round and return to start screen
        if self.backbutton.collidepoint(mouse.get_pos()):
            self.game.action = self.game.get_startscreen()

        else:
            if self.active_mole.active.collidepoint(mouse.get_pos()) and not self.active_mole.hit:
                # if a player hits a active mole, add +1 to score and vanish mole
                self.score += 1
                self.active_mole.hit = True

            self.update()

