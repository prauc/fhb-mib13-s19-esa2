from pygame import *

import pygame


class Moles:
    """Moles Class
        load all moles with their different states

        Args:
            game (Game): CI-Container of Game-Class
    """
    def __init__(self, game):
        # load mole graphic
        mole = pygame.image.load(game.config['match'].get('mole'))
        # subsurface mole states
        self.states = [
            mole.subsurface(0, 0, 108, 66),
            mole.subsurface(119, 0, 108, 66),
            mole.subsurface(248, 0, 108, 66),
            mole.subsurface(380, 0, 108, 66),
            mole.subsurface(503, 0, 108, 66),
            mole.subsurface(623, 0, 108, 66),
            mole.subsurface(735, 0, 108, 66)
        ]

    # get mole graphic depending on state
    def get_mole_by_state(self, state):
        return self.states[state]
