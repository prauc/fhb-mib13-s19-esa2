from pygame import *

from .base import Base

import pygame


class Startscreen(Base):
    """Startscreen Class
        this is used for the startscreen.

        Args:
            game (Game): DI-Container of Game-Class
    """

    def __init__(self, game):
        self.game = game
        self.background = pygame.image.load(game.config['startscreen'].get('background'))
        self.font = game.fonts.get('AR48')
        # button to start a new match
        self.button = None

        # draw all necessary objects on pitch
        self.draw_welcome()
        self.draw_highscore()
        self.draw_start()

        game.screen.blit(self.background, (0, 0))
        pygame.display.update()

    # draw welcome message
    def draw_welcome(self):
        text = self.font.render("Welcome to {}!".format(self.game.name), True, Base.WHITE)

        rect = text.get_rect()
        rect.centerx = self.background.get_rect().centerx
        rect.centery = 100

        self.background.blit(text, rect)

    # draw current highscore
    def draw_highscore(self):
        text = self.font.render("Highscore: {}".format(self.game.highscore), True, Base.WHITE)

        rect = text.get_rect()
        rect.centerx = self.background.get_rect().centerx
        rect.centery = 200

        self.background.blit(text, rect)

    # draw start button
    def draw_start(self):
        self.button = pygame.draw.rect(self.background, Base.BORDEAUX, (self.background.get_rect().centerx-100, self.background.get_rect().centery, 200, 100))
        text = self.font.render("Start", True, Base.WHITE)

        rect = text.get_rect()
        rect.centerx = self.button.centerx
        rect.centery = self.button.centery

        self.background.blit(text, rect)

    def click(self, mouse):
        # if a player clicks, start a new match
        if self.button.collidepoint(mouse.get_pos()):
            self.game.started = True
            self.game.action = self.game.get_match()


